import { LitElement, html, css } from "lit-element";
import "../components/header-component.js";
import "../components/shoe-card-detail.js";
export class CardDetailPage extends LitElement {
  static get properties() {
    return {
      params: { type: Object },
      shoe: { type: Object },
    };
  }

  constructor() {
    super();
    this.params = {};
    this.shoe = {};
  }
  firstUpdated() {
    fetch(
      `https://my-json-server.typicode.com/claumartinezh/training-db/shoes/${this.params.id}`
    )
      .then((r) => r.json())
      .then((d) => this.handleData(d));
  }

  handleData(data) {
    this.shoe = { ...data };
    console.log(this.shoe);
  }

  render() {
    return html` <header-component> </header-component>
      <shoe-card-detail .shoe=${this.shoe}></shoe-card-detail>`;
  }
}

customElements.define("card-detail-page", CardDetailPage);
