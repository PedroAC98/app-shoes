import { LitElement, html, css } from "lit-element";
import "../components/header-component.js";
import "../components/shoe-list.js";
import "../components/filters-component.js";
export class HomePage extends LitElement {
  static get properties() {
    return {
      response: { type: Array },
      category: { type: String },
      brand: { type: String },
      size: { type: Number },
      filteredList: { type: Array },
    };
  }

  constructor() {
    super();
    this.category = "";
    this.size = undefined;
    this.brand = "";
    this.response = [];
    this.filteredList = [];
  }

  firstUpdated() {
    fetch("https://my-json-server.typicode.com/claumartinezh/training-db/shoes")
      .then((r) => r.json())
      .then((d) => this.handleData(d));
  }
  handleData(data) {
    this.response = [...data];
    this.filteredList = [...data];
  }

  static get styles() {
    return css`
      .page-container {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        justify-content: center;
      }
      .main-container {
        display: flex;
        flex-direction: row;
      }
    `;
  }

  render() {
    return html`
      <div class="page-container">
        <header-component> </header-component>
        <main class="main-container">
          <filters-component
            @filter-sizes=${(e) => this._filterSize(e)}
            @filter-categories=${(e) => this._filterCategorie(e)}
            @filter-brands=${(e) => this._filterBrand(e)}
          ></filters-component>
          <shoe-list .shoeList=${this.filteredList}></shoe-list>
        </main>
      </div>
    `;
  }
  _filterSize(e) {
    this.size = e.detail;
    this._filters();
  }
  _filterBrand(e) {
    this.brand = e.detail;
    this._filters();
  }
  _filterCategorie(e) {
    this.category = e.detail;
    this._filters();
  }
  _filters() {
    this.filteredList = this.response;
    if (this.category) {
      this.filteredList = this.filteredList;
      this.filteredList = this.filteredList.filter(
        (shoe) => shoe.category === this.category
      );
    }
    if (this.size) {
      this.filteredList = this.filteredList;
      this.filteredList = this.filteredList.filter((shoe) =>
        shoe.size.find((element) => element === this.size)
      );
    }
    if (this.brand) {
      this.filteredList = this.filteredList;
      this.filteredList = this.filteredList.filter(
        (shoe) => shoe.brand === this.brand
      );
    }
  }
}

customElements.define("home-page", HomePage);
