import { LitElement, html, css } from "lit";
import "../pages/home-page";
import "./app-main.js";
import "./app-link.js";
import "../pages/card-detail-page.js";
import { router } from "lit-element-router";

export class ShoeMarketApp extends router(LitElement) {
  static get styles() {
    return css``;
  }
  static get properties() {
    return {
      route: { type: String },
      params: { type: Object },
      query: { type: Object },
    };
  }
  static get routes() {
    return [
      {
        name: "home",
        pattern: "/",
        data: { title: "Home" },
      },
      {
        name: "shoe",
        pattern: "shoe/:id",
      },
      {
        name: "not-found",
        pattern: "*",
      },
    ];
  }

  constructor() {
    super();
    this.route = "";
    this.params = {};
    this.query = {};
  }
  router(route, params, query) {
    this.route = route;
    this.params = params;
    this.query = query;
  }

  render() {
    return html`
      <app-main active-route=${this.route}>
        <h1 route="home"><home-page></home-page></h1>

        <h1 route="shoe">
          ${this.params.id === undefined
            ? ""
            : html`<card-detail-page
                .params=${this.params}
              ></card-detail-page>`}
        </h1>
        <h1 route="not-found">Not Found</h1>
      </app-main>
    `;
  }
}

customElements.define("shoe-market-app", ShoeMarketApp);
