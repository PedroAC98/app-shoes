import { LitElement, html, css } from "lit-element";
import "./ShoeMarketApp.js";
import "../components/header-component.js";

export class ShoeApp extends LitElement {
  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  static get styles() {
    return css`
      :host {
        display: block;
        box-sizing: border-box;
        font-family: Arial, Helvetica, sans-serif;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }

      h1 {
        padding-top: 20px;
        margin-top: 20px;
        border-top: 2px solid #1973b8;
        font-size: 28px;
        color: #1973b8;
      }
    `;
  }

  render() {
    return html` <shoe-market-app> </shoe-market-app> `;
  }
}

customElements.define("shoe-app", ShoeApp);
