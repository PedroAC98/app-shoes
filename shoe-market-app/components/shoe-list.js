import { LitElement, html, css } from "lit-element";
import "./shoe-card.js";
export class ShoeList extends LitElement {
  static get properties() {
    return {
      shoeList: { type: Array },
    };
  }

  constructor() {
    super();

    this.shoeList = [];
  }

  static get styles() {
    return css`
      .list-container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        list-style: none;
        justify-content: center;
      }
    `;
  }

  render() {
    return html`<ul class="list-container">
      ${this.shoeList.map(
        (shoeCard, id) =>
          html`<li>
            <app-link href="shoe/${shoeCard.id}">
              <shoe-card .shoe=${shoeCard}></shoe-card
            ></app-link>
          </li>`
      )}
    </ul>`;
  }
}

customElements.define("shoe-list", ShoeList);
