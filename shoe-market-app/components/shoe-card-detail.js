import { LitElement, html, css } from "lit-element";
export class ShoeCardDetail extends LitElement {
  static get properties() {
    return {
      shoe: { type: Object },
    };
  }

  constructor() {
    super();
    this.shoe = {};
  }

  static get styles() {
    return css``;
  }

  render() {
    return html`
      <div class="card-container">
        <img class="img" src="${this.shoe.image}" />
        <button>&#xf015;</button>
        <p class="shoe-name">${this.shoe.name}</p>
        <p class="shoe-price">${this.shoe.price}</p>
      </div>
    `;
  }
}

customElements.define("shoe-card-detail", ShoeCardDetail);
