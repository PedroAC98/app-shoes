import { LitElement, html, css } from "lit-element";

export class HeaderComponent extends LitElement {
  static get styles() {
    return css`
      .header-container {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        background-color: gray;
        align-items: center;
      }
    `;
  }
  render() {
    return html`
      <div class="header-container">
        <app-link class="link-decoration" href="/">Home</app-link>
        <p>Men´s Lifestyle Sneakers</p>
        <app-link href="/cart">Cart</app-link>
      </div>
    `;
  }
}

customElements.define("header-component", HeaderComponent);
