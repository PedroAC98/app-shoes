import { LitElement, html, css } from "lit-element";

export class FiltersComponent extends LitElement {
  static get properties() {
    return {
      categories: { type: Array },
      sizes: { type: Array },
      brands: { type: Array },
      filteredCategorie: { type: String },
      filteredSize: { type: Number },
      filteredBrand: { type: String },
    };
  }

  constructor() {
    super();
    this.categories = ["Basketball", "Tennis", "Skate", "Hiking"];
    this.sizes = [38, 39, 40, 41, 42, 43, 44, 45, 46];
    this.brands = [
      "Nike",
      "Jordan",
      "Lacoste",
      "Converse",
      "Vans",
      "Salomon",
      "Timberland",
    ];
    this.filteredCategorie = "";
    this.filteredBrand = "";
  }
  static get styles() {
    return css`
      .filters-container {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        justify-content: center;
        margin-top: 2rem;
        margin-left: 1.5rem;
      }
      .categories-container {
        list-style: none;
        font-size: 1rem;
        margin-left: 1rem;
        margin-top: 0rem;
      }
      .size-container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
      }
      .div-size-container {
        font-weight: 200;
        font-size: 1.2rem;
        width: 22%;
        padding-left: 0.3rem;
        margin: 0.1rem;
        border-style: solid;
        border-width: 2px;
        border-color: #a6a6a6;
      }
      .div-size-container:hover {
        background-color: #b3ccff;
        cursor: pointer;
      }
      .div-size-container-clicked {
        background-color: #006699;
      }
      h3 {
        margin-bottom: 0rem;
      }
      h6 {
        margin-top: 1rem;
        margin-bottom: 0.7rem;
      }
      li {
        margin-bottom: 0.3rem;
      }
      li:hover {
        background-color: #b3ccff;
        cursor: pointer;
      }
      .li-category-focused {
        background-color: #006699;
      }
      .li-brand-focused {
        background-color: #006699;
      }
    `;
  }

  render() {
    return html`
      <div class="filters-container">
        <h3>Filters</h3>
        <h6>Categories</h6>
        <div class="categories-container">
          ${this.categories.map(
            (category, id) =>
              html`<li
                class=${this.filteredCategorie === category
                  ? "li-category-focused"
                  : ""}
                id=${id}
                @click=${this._categoryClicked}
              >
                ${category}
              </li>`
          )}
        </div>
        <h6>Size</h6>
        <div class="size-container">
          ${this.sizes.map(
            (size, id) =>
              html`<div
                @click=${this._sizeClicked}
                id=${id}
                class="div-size-container + ${this.filteredSize === size
                  ? "div-size-container-clicked"
                  : ""} "
              >
                ${size}
              </div>`
          )}
        </div>
        <h6>Brand</h6>
        <div class="categories-container">
          ${this.brands.map(
            (brand, id) =>
              html`<li
                class=${this.filteredBrand === brand ? "li-brand-focused" : ""}
                id=${id}
                @click=${this._brandClicked}
              >
                ${brand}
              </li>`
          )}
        </div>
      </div>
    `;
  }
  _brandClicked(e) {
    console.log(e.target.id);
    this.filteredBrand = this.brands[e.target.id];

    if (e.target.classList.contains("li-brand-focused")) {
      e.target.classList.remove("li-brand-focused");
      this.filteredBrand = null;
    }
    this.dispatchEvent(
      new CustomEvent("filter-brands", {
        detail: this.filteredBrand,
        bubbles: true,
      })
    );
  }
  _sizeClicked(e) {
    this.filteredSize = this.sizes[e.target.id];
    if (e.target.classList.contains("div-size-container-clicked")) {
      e.target.classList.remove("div-size-container-clicked");
      this.filteredSize = null;
    }
    this.dispatchEvent(
      new CustomEvent("filter-sizes", {
        detail: this.filteredSize,
        bubbles: true,
      })
    );
  }
  _categoryClicked(e) {
    this.filteredCategorie = this.categories[parseInt(e.target.id)];
    if (e.target.classList.contains("li-category-focused")) {
      e.target.classList.remove("li-category-focused");
      this.filteredCategorie = null;
    }
    this.dispatchEvent(
      new CustomEvent("filter-categories", {
        detail: this.filteredCategorie,
        bubbles: true,
      })
    );
  }
}

customElements.define("filters-component", FiltersComponent);
