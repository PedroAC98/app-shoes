import { LitElement, html, css } from "lit-element";
export class ShoeCard extends LitElement {
  static get properties() {
    return {
      shoe: { type: Object },
    };
  }

  constructor() {
    super();
    this.shoe = {
      image:
        "https://images.footlocker.com/is/image/FLEU/314103214004_01?wid=520&hei=520&fmt=png-alpha",
    };
  }

  static get styles() {
    return css`
      .card-container {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        min-width: 17rem;
        height: 20rem;
        align-items: center;
        border-style: solid;
        border-width: 4px 4px 4px 4px;
        border-color: black;
        border-radius: 0.3rem;
        margin-left: 2rem;
        margin-top: 1rem;
      }
      .img {
        width: 13rem;
        margin-bottom: 0rem;
        margin-top: 2rem;
      }
      .shoe-name {
        font-size: 1rem;
        margin-bottom: 0rem;
        text-decoration-style: none;
      }
      .shoe-price {
        font-size: 0.8rem;
        margin-bottom: 0.4rem;
        color: #7c136d;
      }
    `;
  }

  render() {
    return html`
      <div class="card-container">
        <img class="img" src="${this.shoe.image}" />
        <p class="shoe-name">${this.shoe.name}</p>
        <p class="shoe-price">${this.shoe.price}</p>
      </div>
    `;
  }
}

customElements.define("shoe-card", ShoeCard);
